'use strict';

// ready
$(document).ready(function() {

    // anchor
    $(".anchor").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top + 40}, 1000);
    });
    // anchor

    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $(this).next().slideToggle();
    });
    // adaptive menu

    // mask phone {maskedinput}
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    $('.slideshow-inner').each(function (idx, item) {
        var carouselId = "carousel" + idx;
        this.id = carouselId;
        $(this).slick({
          slide: "#" + carouselId +" .option",
          appendArrows: "#" + carouselId + " .slider-arrow",
          nextArrow: '<div class="slider-arrow-left slick-arrow"><i class="is-icons is-icons--slide-next"></i></div>',
          prevArrow: '<div class="slider-arrow-right slick-arrow"><i class="is-icons is-icons--slide-prev"></i></div>'
        });
    });
    $('.catalog-inner, .schedule, .menu-catalog').slick({
      mobileFirst: true,
      dots: true,
      arrows: false,
      infinite: false,
      responsive: [
        {
          breakpoint: 769,
          settings: "unslick"
        }
      ]
    });
    var filterSlider = $('.filtering');
    filterSlider.slick({
      slide: ".option",
      appendArrows: ".slider-arrow",
      nextArrow: '<div class="slider-arrow-left slick-arrow"><i class="is-icons is-icons--slide-next"></i></div>',
      prevArrow: '<div class="slider-arrow-right slick-arrow"><i class="is-icons is-icons--slide-prev"></i></div>'
    });
    $('.js-filter').on('click', function(){
      $(this).siblings().removeClass('active');
      filterSlider.slick('slickUnfilter');
      filterSlider.slick('slickFilter',':even');
      $(this).addClass('active');
      console.log('even on');
    });
    $('.js-filter-1').on('click', function(){
      $(this).siblings().removeClass('active');
      filterSlider.slick('slickUnfilter');
      filterSlider.slick('slickFilter',':odd');
      $(this).addClass('active');
      console.log('odd on');
    });
    $('.js-filter-2').on('click', function(){
      $(this).siblings().removeClass('active');
      filterSlider.slick('slickUnfilter');
      $(this).addClass('active');
      console.log('off');
      return false;
    });
    // slider

    // select {select2}
    //$('select').select2({});
    // select



    // popup {magnific-popup}
    $('.popup').magnificPopup({
        showCloseBtn:false,
        mainClass: 'mfp-fullheight'
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
    $('.popup-youtube').magnificPopup({
  		disableOn: 700,
  		type: 'iframe',
  		mainClass: 'mfp-fade',
  		removalDelay: 160,
  		preloader: false,

  		fixedContentPos: false
  	});
    // popup

    //tabs
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
      $(this)
        .addClass('active').siblings().removeClass('active')
        .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    //tabs

    //fullpage
    $('#fullpage').fullpage({
        hybrid:true,
        fitToSection: false,
        anchors: ['firstPage', 'secondPage', '3rdPage', '4rdPage', '5rdPage', '6rdPage', 'footer'],
  		  scrollingSpeed: 350,
	      navigation: true,
        navigationPosition: 'left',
        responsiveWidth: 1199
    });
    //fullpage

    //full-screen
    function alturaMaxima() {
        var altura = $(window).height();
        $(".full-screen").css('height', altura);
    }
    alturaMaxima();
    $(window).bind('resize', alturaMaxima);
    //full-screen



    //calendar
    if($("#calendars").length) {
      var plus_5_days = new Date;
      var element = document.getElementById("calendars");
    	plus_5_days.setDate(plus_5_days.getDate() + 5);
    	pickmeup(element, {
    		flat      : true,
    		date      : [
    			new Date,
    			plus_5_days
    		],
        prev      : '<i class="is-icons is-icons--cal-pr"></i>',
        next      : '<i class="is-icons is-icons--cal-nx"></i>',
    		mode      : 'range',
        locale    : 'ru',
    		locales   : {
    			ru : {
            days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
          	daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
          	daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
          	months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
          	monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
    			}
    		},
    		calendars : 2
    	});
      element.addEventListener('pickmeup-change', function (e) {
          $('.return_date').html('С ' + e.detail.formatted_date[0] + ' до ' + e.detail.formatted_date[1]);
      })
    }
    //calendar
});
// ready


//map
function initMap() {
    var mapOptions = {
        center: new google.maps.LatLng(59.91916157, 30.3251195),
        zoom: 16,
        mapTypeControl: false,
        zoomControl: false,
        scrollwheel: false
    };
    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(59.91916157, 30.3251195),
        map: map,
        icon: "../images/icons/bubble.svg"
    });
}
//map


// mobile sctipts
// mobile sctipts
